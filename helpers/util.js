const moment = require('moment');

module.exports = {
	getRandomText: function (size) {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < size; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	},

	buildSuccessResponse: function (data) {
		return {
			success: true,
			data: data
		}
	},
	buildErrorResponse: function (err, req, message) {
		var errRespose = {};
		errRespose.success = false;

		if (message) errRespose.message = message;
		errRespose.body = req.body
		var errors = []
		if (err) {
			console.error("ERROR====== " + moment().format());
			console.error(err);
			errRespose.message = err.message;
			if (err.errors) {
				console.log("ERRORS======");
				console.log(err.errors);
				for (var error in err.errors) {
					var prop = err.errors[error];
					errors.push({
						type: err.name,
						field: prop.path,
						value: prop.value,
						kind: prop.kind || prop.properties.kind,
						message: prop.message
					});
				}
			} else {
				errors.push({type: err.name, code: err.code, message: err.message});
			}
		}
		errRespose.errors = errors;
		return errRespose;
	},

	databaseReadValidation: function (err, req, records, callback) {
		if (err) {
			console.error(err);
			return callback(module.exports.buildErrorResponse(err, req));
		}else if (!records)
			return callback(module.exports.buildErrorResponse(null, req, 'Not records found'));
		else
			return callback(null, records)
	},

	waterfallCallback: function (err,result,res) {
		if(err) return res.json(err);
		else return res.json(result);
	},

	debugRequest: function (req, res, next) {
		console.log(req.method + " => " + req.originalUrl);
		console.log("BODY===========");
		console.log(req.body);
		next();
	}

}
