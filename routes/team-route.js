const express = require('express');
const TeamMid = require('../middlewares/team-mid');
const router = express.Router();

router.get('/',[TeamMid.getTeams]);
router.get('/:id',[TeamMid.getTeam]);
router.post('/',[TeamMid.addTeam]);
router.delete('/:id',[TeamMid.deleteTeam]);
router.put('/reset',[TeamMid.resetStats]);
router.put('/:id',[TeamMid.updateTeam]);


module.exports = router;