const express = require('express');
const util = require ('../helpers/util');
const api = express.Router();

api.use(util.debugRequest);
api.use('/team', require('./team-route'));
api.use('/match', require('./match-route'));


module.exports = api;