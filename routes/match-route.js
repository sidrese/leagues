const express = require('express');
const MatchMid = require('../middlewares/match-mid');
const router = express.Router();

router.get('/',[MatchMid.getActiveMatches]);
router.post('/generate',[MatchMid.generateMatches,MatchMid.getActiveMatches]);
router.put('/finish/:id',[MatchMid.finalizeMatch]);


module.exports = router;