const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MatchSchema = new Schema({
	team_1: {type: Schema.Types.ObjectId, ref: 'Team', required: true},
	team_2: {type: Schema.Types.ObjectId, ref: 'Team', required: true},
	score_1: {type: Number, default: 0},
	score_2: {type: Number, default: 0},
	finalized: {type: Boolean, default: false},
	active: {type: Boolean, default: true}
});

module.exports = mongoose.model('Match', MatchSchema);
