const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PlayerSchema = new Schema({
	name: {type: String, required: true, trim: true, index: true, unique: true},
},{_id:false});

const TeamSchema = new Schema({
	name: {type: String, required: true, trim: true, index: true, unique: true},
	players: {type: [PlayerSchema]},
	played: {type: Number, default: 0},
	won: {type: Number, default: 0},
	lost: {type: Number, default: 0},
	score_positive: {type: Number, default: 0},
	score_negative: {type: Number, default: 0},
});

module.exports = mongoose.model('Team', TeamSchema);
