const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const config = require('./config/' + process.env.NODE_ENV);

//const index = require('./routes/index');
//const users = require('./routes/users');

const app = express();

mongoose.connect(config.database.url,config.database.options);

app.use(logger('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());
app.use(function (req,res,next) {
	if(req.method == 'OPTIONS'){
		res.send(200);
	} else {
		next();
	}
});
/*
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

app.use(function (err, req, res, next) {

	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	res.status(err.status || 500);
	res.json({code:err.status || 500});
});*/

app.use('/api', require('./routes/api'));

module.exports = app;
