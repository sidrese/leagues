const Team = require('../models/team');
const util = require('../helpers/util');
const async = require('async');

module.exports = {

	getTeams: function (req, res, next) {
		async.waterfall([
			function (callback) {
				Team.find({}).lean().exec(function (err, teams) {
					util.databaseReadValidation(err, req, teams, callback);
				})
			},
			function (teams, callback) {
				callback(null, util.buildSuccessResponse({teams: teams}));
			}
		], function (err, result) {
			util.waterfallCallback(err, result, res);
		});
	},

	getTeam: function (req, res, next) {
		async.waterfall([
			function (callback) {
				Team.findById(req.params.id).lean().exec(function (err,team) {
					util.databaseReadValidation(err, req, team, callback);
				});
			},
			function (team, callback) {
				callback(null,util.buildSuccessResponse({team: team}));
			}
		],function (err,result) {
			util.waterfallCallback(err,result,res);
		});

	},

	addTeam: function (req, res, next) {
		let team = new Team();
		team.name = req.body.name;
		team.players = req.body.players;

		team.save(function (err) {
			if (err) res.json(util.buildErrorResponse(err, req));
			else res.json(util.buildSuccessResponse({team: team}));
		});

	},

	updateTeam: function (req, res, next) {
		Team.update({_id: req.params.id},req.body,function (err) {
			if (err) res.json(util.buildErrorResponse(err, req));
			else res.json(util.buildSuccessResponse({}));
		});

	},

	deleteTeam: function (req, res, next) {

		Team.findByIdAndRemove(req.params.id, function (err, team) {
			if (err) res.json(util.buildErrorResponse(err, req));
			else res.json(util.buildSuccessResponse({}));
		});

	},

	resetStats: function (req, res, next) {
		Team.update({},{
			score_negative: 0,
			score_positive: 0,
			won: 0,
			lost: 0,
			played: 0
		},{multi:true},function (err) {
			if (err) res.json(util.buildErrorResponse(err, req));
			else res.json(util.buildSuccessResponse({}));
		});
	}

};