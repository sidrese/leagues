const Match = require('../models/match');
const Team = require('../models/team');
const util = require('../helpers/util');
const mongoose = require('mongoose');
const async = require('async');

module.exports = {

	getActiveMatches: function (req, res, next) {
		async.waterfall([
			function (callback) {
				Match.find({active: true}).populate('team_1',{name:1,players:1}).populate('team_2',{name:1,players:1}).lean().exec(function (err, matches) {
					util.databaseReadValidation(err, req, matches, callback);
				})
			},
			function (matches, callback) {
				callback(null, util.buildSuccessResponse({matches: matches}));
			}
		], function (err, result) {
			util.waterfallCallback(err, result, res);
		});
	},

	finalizeMatch: function (req, res, next) {
		async.waterfall([
			function (callback) {
				Match.findById(req.params.id).populate('team_1').populate('team_2').exec(function (err, match) {
					util.databaseReadValidation(err, req, match, callback);
				})
			},
			function (match, callback) {
				match.score_1 = req.body.score_1;
				match.score_2 = req.body.score_2;
				match.finalized = true;

				match.team_1.played ++;
				match.team_1.score_positive = match.team_1.score_positive+req.body.score_1;
				match.team_1.score_negative = match.team_1.score_negative+req.body.score_2;

				match.team_2.played ++;
				match.team_2.score_positive = match.team_2.score_positive+req.body.score_2;
				match.team_2.score_negative = match.team_2.score_negative+req.body.score_1;

				if(req.body.score_1 > req.body.score_2) {
					match.team_1.won ++;
					match.team_2.lost ++;
				} else if (req.body.score_1 < req.body.score_2){
					match.team_2.won ++;
					match.team_1.lost ++;
				}

				match.save(function (err) {
					if (err) callback(err, null);
					else {
						match.team_1.save();
						match.team_2.save();
						callback(null, util.buildSuccessResponse({}));
					}
				});
			}
		], function (err, result) {
			util.waterfallCallback(err, result, res);
		});
	},

	generateMatches: function (req, res, next) {
		async.waterfall([
			function (callback) {
				Match.update({finalized: true}, {active: false}, {multi: true}, function (err, raw) {
					if (err) callback(err);
					else callback(null);
				});
			},
			function (callback) {
				let selection = {};
				if (req.body.ids && req.body.ids.length > 0) {
					let ids = req.body.ids.map(function (id) {
						return mongoose.Types.ObjectId(id);
					});
					selection = {"_id": {$in:ids}};
				}
				Team.find(selection).lean().exec(function (err, teams) {
					util.databaseReadValidation(err, req, teams, callback);
				})
			},
			function (teams, callback) {
				let teams_ids = teams.map(function (team) {
					return team._id;
				});

				let teams_ids_copy = teams_ids.slice(0);

				let matches = [];

				while (teams_ids.length > 0) {
					let index_1 = Math.floor(Math.random() * teams_ids.length);
					let index_2 = 0;

					if(teams_ids.length == 1){
						let id_1 = teams_ids[index_1];
						let id_2 = teams_ids[index_1];
						while (id_1 == id_2) {
							id_2 = teams_ids_copy[Math.floor(Math.random() * teams_ids_copy.length)];
						}
						console.log(id_1 +'=='+ id_2);
						matches.push(new Match({
							team_1: id_1,
							team_2: id_2
						}));

						teams_ids.splice(0, 1);
					} else {
						while (index_1 == index_2) {
							index_2 = Math.floor(Math.random() * teams_ids.length);
						}

						let id_1 = teams_ids[index_1];
						let id_2 = teams_ids[index_2];

						matches.push(new Match({
							team_1: id_1,
							team_2: id_2
						}));

						let index = null;
						index = teams_ids.findIndex(function (id) {
							return id_1 == id;
						})
						teams_ids.splice(index, 1);
						index = teams_ids.findIndex(function (id) {
							return id_2 == id;
						})
						teams_ids.splice(index, 1);
					}

				}
				console.log(matches);
				Match.create(matches, function (err) {
					callback(err, {});
				})


			}
		], function (err, result) {
			if (err) return res.json(err);
			else next();
		});
	}

}